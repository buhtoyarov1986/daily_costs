# frozen_string_literal: true

RSpec.describe Calculating do
  before do
    # Freeze time, because tests will break if the month is not 31 days.
    Timecop.freeze(Time.local(2019, 1, 1))
  end

  after do
    Timecop.return
  end

  describe '#daily_costs' do
    subject(:daily_costs) { described_class.daily_costs(start_date, end_date, time_period_costs) }

    let(:all_time_periods) { %w[daily weekly monthly] }

    context 'single time period' do
      let(:start_date) { Date.new(2019, 10, 1) }
      let(:end_date) { Date.new(2019, 10, 1) }
      let(:time_period_costs) do
        [
          {
            time_period: all_time_periods[0], # daily
            cost: 10.0
          }
        ]
      end

      it 'should return a curret answer' do
        expect(daily_costs).to include(
          cost: 10.0, date: '2019-10-01'
        )
      end
    end

    context 'multiple time period' do
      let(:start_date) { Date.new(2019, 10, 1) }
      let(:end_date) { Date.new(2019, 10, 3) }
      let(:time_period_costs) do
        [
          {
            time_period: all_time_periods[0], # daily
            cost: 10.0
          }
        ]
      end

      let(:daily_costs) { described_class.daily_costs(start_date, end_date, time_period_costs) }

      it 'should return a curret answer' do
        expect(daily_costs).to include(
          { cost: 10.0, date: '2019-10-01' },
          { cost: 10.0, date: '2019-10-02' },
          cost: 10.0, date: '2019-10-03'
        )
      end
    end

    context 'single period' do
      let(:start_date) { Date.new(2019, 10, 1) }
      let(:end_date) { Date.new(2019, 10, 3) }
      let(:time_period_costs) do
        {
          time_period: all_time_periods[0], # daily
          cost: 10.0
        }
      end 
      
      it 'should return a curret answer' do
        expect(daily_costs).to include(
          { cost: 10.0, date: '2019-10-01' },
          { cost: 10.0, date: '2019-10-02' },
          cost: 10.0, date: '2019-10-03'
        )
      end
    end

    context 'mixed periods' do
      context 'daily and weekly' do
        let(:start_date) { Date.new(2019, 10, 1) }
        let(:end_date) { Date.new(2019, 10, 3) }
        let(:time_period_costs) do
          [
            {
              time_period: all_time_periods[0], # daily
              cost: 10.0
            },
            {
              time_period: all_time_periods[1], # weekly
              cost: 70.0
            }
          ]
        end

        it 'should return a curret answer' do
          expect(daily_costs).to include(
            { cost: 20.0, date: '2019-10-01' },
            { cost: 20.0, date: '2019-10-02' },
            cost: 20.0, date: '2019-10-03'
          )
        end
      end

      context 'daily and monthly' do
        let(:start_date) { Date.new(2019, 10, 1) }
        let(:end_date) { Date.new(2019, 10, 3) }
        let(:time_period_costs) do
          [
            {
              time_period: all_time_periods[0], # daily
              cost: 10.0
            },
            {
              time_period: all_time_periods[2], # monthly
              cost: 280.0
            }
          ]
        end

        it 'should return a curret answer' do
          expect(daily_costs).to include(
            { cost: 19.03, date: '2019-10-01' },
            { cost: 19.03, date: '2019-10-02' },
            cost: 19.03, date: '2019-10-03'
          )
        end
      end

      context 'daily and weekly and monthly' do
        let(:start_date) { Date.new(2019, 10, 1) }
        let(:end_date) { Date.new(2019, 10, 3) }
        let(:time_period_costs) do
          [
            {
              time_period: all_time_periods[0], # daily
              cost: 10.0
            },
            {
              time_period: all_time_periods[1], # weekly
              cost: 70.0
            },
            {
              time_period: all_time_periods[2], # monthly
              cost: 280.0
            }
          ]
        end

        it 'should return a curret answer' do
          expect(daily_costs).to include(
            { cost: 29.03, date: '2019-10-01' },
            { cost: 29.03, date: '2019-10-02' },
            cost: 29.03, date: '2019-10-03'
          )
        end
      end

      context 'when the wrong time period' do
        let(:start_date) { Date.new(2019, 10, 1) }
        let(:end_date) { Date.new(2019, 10, 3) }

        let(:time_period_costs) do
          [
            {
              time_period: 'annually',
              cost: 10.0
            }
          ]
        end

        it 'raise error' do
          expect { daily_costs }.to raise_error 'Invalid time period for cost 10.0'
        end
      end

      context 'when wrong time period' do
        let(:start_date) { Date.new(2019, 10, 1) }
        let(:end_date) { Date.new(2019, 10, 3) }

        let(:time_period_costs) do
          [
            {
              time_period: 'annually',
              cost: 10.0
            }
          ]
        end

        it 'raise error' do
          expect { daily_costs }.to raise_error 'Invalid time period for cost 10.0'
        end
      end

      context 'when cost not positive' do
        let(:start_date) { Date.new(2019, 10, 1) }
        let(:end_date) { Date.new(2019, 10, 3) }

        let(:time_period_costs) do
          [
            {
              time_period: 'daily',
              cost: -10.0
            }
          ]
        end

        it 'raise error' do
          expect { daily_costs }.to raise_error 'Cost should be a positive number. You have passed the cost = -10.0'
        end
      end

      context 'when cost not numeric' do
        let(:start_date) { Date.new(2019, 10, 1) }
        let(:end_date) { Date.new(2019, 10, 3) }

        let(:time_period_costs) do
          [
            {
              time_period: 'daily',
              cost: '$10.0'
            }
          ]
        end

        it 'raise error' do
          expect { daily_costs }.to raise_error 'Cost should be a positive number. You have passed the cost = $10.0'
        end
      end

      context 'when the dates are wrong' do
        let(:start_date) { Date.new(2019, 10, 3) }
        let(:end_date) { Date.new(2019, 10, 1) }

        let(:time_period_costs) do
          [
            {
              time_period: 'daily',
              cost: 10.0
            }
          ]
        end

        it 'raise error' do
          expect { daily_costs }.to raise_error 'The start date must be greater than the end date'
        end
      end
    end
  end
end
