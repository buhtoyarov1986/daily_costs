# frozen_string_literal: true

require 'date'

# Helper class for calculating daily costs.
class Calculating
  class << self
    # @param start_date [Date]
    # @param end_date [Date]
    # @param time_period_costs [Array] An array of hashes. Each hash contains a time_period and cost.
    # @return [Array] the method returns an array of hashes
    def daily_costs(start_date, end_date, time_period_costs)
      raise 'The start date must be greater than the end date' if start_date > end_date

      # for each day in the interval between the start date and the end date.
      # memo - variable accumulates dates and costs for each day.
      # date - current date between time interval
      (start_date..end_date).each_with_object([]) do |date, memo|
        total_cost = 0 # default value cost for current date

        # Iteration of each time period
        [time_period_costs].flatten.each do |time_period_cost|
          cost = time_period_cost[:cost] # time period cost

          # cost must belong to the Numeric class and be a positive number
          unless cost.is_a?(Numeric) && cost.positive?
            raise "Cost should be a positive number. You have passed the cost = #{cost}"
          end

          # calculation of the cost of the day for the current time period
          # total_cost - total cost accumulates the amount for all time periods
          # if the time period does not match the condition, the system will raise an error
          case time_period_cost[:time_period].to_s
          when 'daily'
            total_cost += cost
          when 'weekly'
            total_cost += cost / 7
          when 'monthly'
            total_cost += cost / last_day_of_month
          else
            raise "Invalid time period for cost #{cost}"
          end
        end

        # add date and cost for the current date in the time interval
        memo << {
          date: date.to_s,
          cost: total_cost.round(2)
        }
      end
    end

    private

    # The method returns the number of days for the current month.
    # @return Integer
    def last_day_of_month
      date_today = Date.today # The current date
      Date.new(date_today.year, date_today.month, -1).day # last day of the current month
    end
  end
end
